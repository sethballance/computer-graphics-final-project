var canvas = document.getElementById("renderCanvas"); // Get the canvas element
var engine = new BABYLON.Engine(canvas, true, { stencil: true }); // Generate the BABYLON 3D engine

import { initializePieces } from './piece';
import Board from './board';

let allPieces = [];
let whiteTeamPieces = 12;
let redTeamPieces = 12;
let camera;
let endOfTurn = false;
const teams = {
    red: 'Red',
    white: 'White'
}
let currentTeam = teams.white;
let board;
let teamTurnIndicator;

var createScene = function () {
    var scene = new BABYLON.Scene(engine);
    var light = new BABYLON.HemisphericLight("hemi", new BABYLON.Vector3(0, 1, 0), scene);
    light.intensity = 1;

    camera = new BABYLON.ArcRotateCamera("ArcRotateCamera", 0, Math.PI / 4, 15, BABYLON.Vector3.Zero(), scene);

    board = new Board(scene);
    allPieces = initializePieces(scene, board.squaresMatrix);

    // Skybox
    var skybox = BABYLON.MeshBuilder.CreateBox("skyBox", {size:1000.0}, scene);
    var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
    skyboxMaterial.backFaceCulling = false;
    skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("https://raw.githubusercontent.com/sballance/BabylonJsTextures/master/skyboxes/park", scene);
    skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;
    skyboxMaterial.disableLighting = true;
    skybox.material = skyboxMaterial;
    skybox.infiniteDistance = true;

    var table = new BABYLON.MeshBuilder.CreateCylinder("cylinder", {
        height: 1,
        diameter: 20,
        tessellation: 30
    }, scene);
    var lightWood = new BABYLON.StandardMaterial("light_wood", scene);
    lightWood.diffuseTexture = new BABYLON.Texture("https://raw.githubusercontent.com/sballance/BabylonJsTextures/master/textures/light_wood.jpg", scene);
    table.material = lightWood;
    table.position.y = board.mesh.position.y - .75;

    setupCameraAnimations();

    teamTurnIndicator = BABYLON.MeshBuilder.CreateSphere("teamTurnIndicator", {}, scene);
    teamTurnIndicator.material = new BABYLON.StandardMaterial("teamTurnIndicator", scene);
    teamTurnIndicator.position = new BABYLON.Vector3(0, 1, 5);

    return scene;
}

var scene = createScene(); //Call the createScene function
centerOnScreen();

startTurn();

function centerOnScreen() {
    if (scene.getEngine().getRenderHeight() > scene.getEngine().getRenderWidth()) {
        camera.fovMode = BABYLON.Camera.FOVMODE_HORIZONTAL_FIXED;
    }
    else {
        camera.fovMode = BABYLON.Camera.FOVMODE_VERTICAL_FIXED;
    }
}

function rotateCameraToTeam() {
    if (currentTeam === teams.red) {
        scene.beginAnimation(camera, 0, 50, false);
    } else {
        scene.beginAnimation(camera, 50, 100, false);
    }
    startTurn();
}

function setTeamIndicatorColor() {
    if (currentTeam === teams.red) {
        teamTurnIndicator.material.diffuseColor = new BABYLON.Color3(.7, 0, 0);
    } else {
        teamTurnIndicator.material.diffuseColor = new BABYLON.Color3(1, 1, 1);
    }
}

function startTurn() {
    setTeamIndicatorColor();
    console.log(`${currentTeam} starting turn`);
    endOfTurn = false;
    let selectedPiece = null;
    // watch for click on team piece
    scene.onPointerUp = function (evt, pickResult) {
        if (pickResult.hit) {
            if (pickResult.pickedMesh.name.startsWith(currentTeam.toLowerCase())) {
                selectedPiece = pickResult.pickedMesh;
                allPieces.forEach(p => {
                    p.unHighlight();
                    if (p.mesh.name === selectedPiece.name) {
                        p.highlight();
                    }
                });
            }

            // watch for click on eligible board spot while available
            if (pickResult.pickedMesh.name === 'Tiled Ground' && selectedPiece) {
                checkEligibleSpot(selectedPiece, pickResult.pickedPoint);
            }
        }
    };
}

function checkEligibleSpot(piece, spot) {
    piece = allPieces.find(searchPiece => {
        return searchPiece.mesh.name === piece.name;
    });

    let clickedSquare;
    let xIndex;
    let zIndex;

    [clickedSquare, xIndex, zIndex] = board.findClosestSquare(spot.x, spot.z);

    let xDifference = piece.getPosition().x - clickedSquare.x;
    let zDifference = piece.getPosition().z - clickedSquare.z;

    if (board.findSquareAllowsPlay(xIndex, zIndex)
        && Math.abs(xDifference) === 1 // will need to update for regular pieces not going backwards
        && Math.abs(zDifference) === 1
        && !clickedSquare.piece
        && (xDifference * piece.direction === 1 || piece.isKing)
    ) {
        // move
        let movedPiece = board.popPieceFromSquareByXZ(piece.getPosition().x, piece.getPosition().z);
        movedPiece.move(board.squaresMatrix, zIndex, xIndex);
        endTurn();
        return;
    }

    let jumpedSquare;
    [jumpedSquare] = board.findSquareByXZ((piece.getPosition().x + clickedSquare.x) / 2, (piece.getPosition().z + clickedSquare.z) / 2);

    if (Math.abs(xDifference) === 2
        && Math.abs(zDifference) === 2
        && !clickedSquare.piece
        && jumpedSquare.piece.color !== piece.color
    ) {
        // remove jumped piece from array
        allPieces = allPieces.filter(filterPiece => {
            return filterPiece.mesh.name !== jumpedSquare.piece.mesh.name;
        });
        // and from scene
        if (jumpedSquare.piece.mesh.name.startsWith(teams.red.toLowerCase())) {
            redTeamPieces--;
        } else {
            whiteTeamPieces--;
        }
        jumpedSquare.piece.mesh.dispose();
        jumpedSquare.piece = null;
        // move
        let movedPiece = board.popPieceFromSquareByXZ(piece.getPosition().x, piece.getPosition().z);
        movedPiece.move(board.squaresMatrix, zIndex, xIndex);
        endOfTurn = true;
    }
}
document.onkeydown = doKeyDown;

function doKeyDown(event) {
    let key = event.key;
    if (key == 'Enter' && endOfTurn) {
        endTurn();
    }
}

function endTurn() {
    allPieces.forEach(p => {
        p.unHighlight()
    });

    // check to see if team won
    if (redTeamPieces === 0 || whiteTeamPieces === 0) {
        console.log(`${currentTeam} won!`);
        endGame();
        return;
    }

    if (currentTeam === teams.white) {
        currentTeam = teams.red;
    } else {
        currentTeam = teams.white;
    }

    rotateCameraToTeam();
}

function endGame() {
    // Create a particle system
    var particleSystem = new BABYLON.ParticleSystem("particles", 2000, scene);

    //Texture of each particle
    particleSystem.particleTexture = new BABYLON.Texture("https://raw.githubusercontent.com/sballance/BabylonJsTextures/master/textures/checkers-king-inverted.png", scene);

    // Where the particles come from
    particleSystem.minEmitBox = new BABYLON.Vector3(4, 1.5, 4); // Starting all from
    particleSystem.maxEmitBox = new BABYLON.Vector3(-4, 1.5, -4); // To...

    // Colors of all particles
    particleSystem.color1 = teamTurnIndicator.material.diffuseColor;
    particleSystem.color2 = teamTurnIndicator.material.diffuseColor;

    // Size of each particle (random between...
    particleSystem.minSize = 0.1;
    particleSystem.maxSize = 0.5;

    // Life time of each particle (random between...
    particleSystem.minLifeTime = 0.3;
    particleSystem.maxLifeTime = 1.5;

    // Emission rate
    particleSystem.emitRate = 200;

    // Blend mode : BLENDMODE_ONEONE, or BLENDMODE_STANDARD
    particleSystem.blendMode = BABYLON.ParticleSystem.BLENDMODE_ONEONE;

    // Set the gravity of all particles
    particleSystem.gravity = new BABYLON.Vector3(0, -9.81, 0);

    // Direction of each particle after it has been emitted
    particleSystem.direction1 = new BABYLON.Vector3(-8, 8, 8);
    particleSystem.direction2 = new BABYLON.Vector3(8, 8, -8);

    // Speed
    particleSystem.minEmitPower = .01;
    particleSystem.maxEmitPower = .5;

    // Start the particle system
    particleSystem.start();
}

function setupCameraAnimations() {
    var myAnimation = new BABYLON.Animation(
        "myAnimation",
        "alpha",
        30,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
    );

    var myAnimation2 = new BABYLON.Animation(
        "myAnimation2",
        "beta",
        30,
        BABYLON.Animation.ANIMATIONTYPE_FLOAT,
        BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE
    );

    var alphaRotations = [];
    alphaRotations.push({
        frame: 0,
        value: camera.alpha
    });

    alphaRotations.push({
        frame: 50,
        value: Math.PI
    });

    alphaRotations.push({
        frame: 100,
        value: 0
    });

    var betaRotations = [];
    betaRotations.push({
        frame: 0,
        value: Math.PI/4
    });

    betaRotations.push({
        frame: 25,
        value: Math.PI/3
    });

    betaRotations.push({
        frame: 50,
        value: Math.PI/4
    });

    betaRotations.push({
        frame: 75,
        value: Math.PI/3
    });

    betaRotations.push({
        frame: 100,
        value: Math.PI/4
    });

    myAnimation.setKeys(alphaRotations);
    myAnimation2.setKeys(betaRotations);

    var easingFunction = new BABYLON.SineEase();
    easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
    myAnimation.setEasingFunction(easingFunction);
    myAnimation2.setEasingFunction(easingFunction);
    camera.animations.push(myAnimation);
    camera.animations.push(myAnimation2);
}

// Register a render loop to repeatedly render the scene
engine.runRenderLoop(function () {
    scene.render();
});

// Watch for browser/canvas resize events
window.addEventListener("resize", function () {
    engine.resize();
}); 

engine.getCaps().highPrecisionShaderSupported = false;
scene.getEngine().onResizeObservable.add(centerOnScreen())