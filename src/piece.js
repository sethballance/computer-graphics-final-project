const teamColor = [
    'red',
    'white'
];

export class Piece {
    constructor(team, scene, x, z) {
        if (isNaN(team) || team > 1 || team < 0) {
            throw "Not a valid team. Must be team 0 or 1";
        }
        this.id = Piece.incrementId();
        this.isKing = false;
        this.setTeam(team);
        this.scene = scene;
        this.height = .15;
        this.diameter = .75;
        this.highlighter = new BABYLON.HighlightLayer("highlighter", this.scene);
        this.buildPiece(x, z).then(myMesh => {
            this.mesh = myMesh;
            this.mesh.name = this.color + this.id;
            let direction = this.team === 0 ? 1 : -1;
            this.mesh.rotation.y = direction * Math.PI / 2;
        });
    }

    static incrementId() {
        if (this.latestId == undefined) {
            this.latestId = 0;
        } else {
            this.latestId++;
        }
        return this.latestId;
    }

    setTeam(team) {
        this.team = team;
        this.color = teamColor[team];
        this.direction = team > 0 ? 1 : -1;
    }

    buildPiece(x, z) {
        return new Promise(resolve => {
            BABYLON.SceneLoader.ImportMeshAsync(
                "",
                "https://raw.githubusercontent.com/sballance/BabylonJsTextures/master/blender_meshes/",
                `checker_piece_${this.color}.babylon`,
                this.scene
            ).then(importedMesh => {
                var checkerPiece = importedMesh.meshes[0];
                checkerPiece.position.x = x;
                checkerPiece.position.y = .1;
                checkerPiece.position.z = z;
                checkerPiece.scaling = new BABYLON.Vector3(.3, .08, .3);
                resolve(checkerPiece)
            });
        });
    }

    move(squares, newRow, newColumn) {
        this.mesh.animations = [];
        squares[newRow][newColumn].piece = this;
        this.addMoveAnimation(
            this.getPosition().x,
            this.getPosition().z,
            squares[newRow][newColumn].x,
            squares[newRow][newColumn].z
        );
        this.checkForKingMe(squares[newRow][newColumn].x);
        this.scene.beginAnimation(this.mesh, 0, 30, false);
    }

    checkForKingMe(newX) {
        if (newX * this.direction === -3.5) {
            this.isKing = true;
            this.addKingAnimation();
        }
    }

    addMoveAnimation(oldX, oldZ, newX, newZ) {
        let startY = this.getPosition().y;
        var moveAnimation = new BABYLON.Animation(
            "moveAnimation",
            "position",
            30,
            BABYLON.Animation.ANIMATIONTYPE_VECTOR3,
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );

        var movePoints = [];
        movePoints.push({
            frame: 0,
            value: new BABYLON.Vector3(oldX, startY, oldZ)
        });
        movePoints.push({
            frame: 8,
            value: new BABYLON.Vector3(oldX, startY+1, oldZ)
        });
        movePoints.push({
            frame: 22,
            value: new BABYLON.Vector3(newX, startY+1, newZ)
        });
        movePoints.push({
            frame: 30,
            value: new BABYLON.Vector3(newX, startY, newZ)
        });

        
        var easingFunction = new BABYLON.SineEase();
        easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        moveAnimation.setEasingFunction(easingFunction);
        moveAnimation.setKeys(movePoints);
        this.mesh.animations.push(moveAnimation);
    }

    addKingAnimation() {        
        var kingAnimation = new BABYLON.Animation(
            "kingAnimation",
            "rotation.z",
            30,
            BABYLON.Animation.ANIMATIONTYPE_FLOAT,
            BABYLON.Animation.ANIMATIONLOOPMODE_CONSTANT
        );

        var rotations = [];
        rotations.push({
            frame: 0,
            value: 0
        });
        rotations.push({
            frame: 8,
            value: 0
        });
        rotations.push({
            frame: 22,
            value: Math.PI
        });
        rotations.push({
            frame: 30,
            value: Math.PI
        });

        var easingFunction = new BABYLON.SineEase();
        easingFunction.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        kingAnimation.setEasingFunction(easingFunction);

        kingAnimation.setKeys(rotations);
        this.mesh.animations.push(kingAnimation);
        
        // this.mesh.scaling.y = 2.5;
    }
    
    getPosition() {
        let x = Math.round(this.mesh.position.x*10)/10;
        let y = Math.round(this.mesh.position.y*10)/10;
        let z = Math.round(this.mesh.position.z*10)/10;

        return {
            x: x,
            y: y,
            z: z
        }
    }

    highlight() {
        this.highlighter.addMesh(this.mesh, BABYLON.Color3.Yellow());
    }

    unHighlight() {
        this.highlighter.removeMesh(this.mesh);
    }
}

export function initializePieces(scene, squares) {
    let pieces = [];
    for (var i = 0; i < 3; i++) {
        for (var j = 0; j < 8; j++) {
            if ((i + j) % 2 === 0) {
                let white = new Piece(0, scene, squares[i][j].x, squares[i][j].z);
                squares[i][j].piece = white;
                pieces.push(white);
            }
        }
    }

    for (var i = 5; i < 8; i++) {
        for (var j = 0; j < 8; j++) {
            if ((i + j) % 2 === 0) {
                let red = new Piece(1, scene, squares[i][j].x, squares[i][j].z);
                squares[i][j].piece = red;
                pieces.push(red);
            }
        }
    }

    return pieces;
}