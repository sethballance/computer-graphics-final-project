export default class Board {
    constructor(scene) {
        this.scene = scene;
        this.tileWidth = 1;
        this.tilesPerRow = 8;
        this.squaresMatrix = this.buildSquaresMatrix();
        this.buildBoard();
    }

    buildBoard() {
        // Tiled ground parameters
        var xmin = -this.tilesPerRow / (this.tileWidth * 2);
        var zmin = xmin;
        var xmax = -xmin;
        var zmax = xmax;
        var subdivisions = {
            'h': this.tilesPerRow, // because of submeshes
            'w': this.tilesPerRow
        };
        // Create the Tiled Ground
        var tiledBoard = BABYLON.MeshBuilder.CreateTiledGround("Tiled Ground", {
            subdivisions: subdivisions,
            xmin: xmin,
            xmax: xmax,
            zmin: zmin,
            zmax: zmax,
        }, this.scene);

        // Create black and white materials and apply
        var blackMaterial = new BABYLON.StandardMaterial("Black", this.scene);
        blackMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
        var whiteMaterial = new BABYLON.StandardMaterial("White", this.scene);
        whiteMaterial.diffuseColor = new BABYLON.Color3(1, 1, 1);
        var multimat = new BABYLON.MultiMaterial("multi", this.scene);
        multimat.subMaterials.push(blackMaterial);
        multimat.subMaterials.push(whiteMaterial);
        tiledBoard.material = multimat;

        // Needed variables to set subMeshes
        var verticesCount = tiledBoard.getTotalVertices();
        var tileIndicesLength = tiledBoard.getIndices().length / (subdivisions.w * subdivisions.h);

        // Set subMeshes of the tiled ground
        tiledBoard.subMeshes = [];
        var base = 0;
        for (var row = 0; row < subdivisions.h; row++) {
            for (var col = 0; col < subdivisions.w; col++) {
                tiledBoard.subMeshes.push(new BABYLON.SubMesh(row % 2 ^ col % 2, 0, verticesCount, base, tileIndicesLength, tiledBoard));
                base += tileIndicesLength;
            }
        }

        var height = .5;
        var boardBase = new BABYLON.MeshBuilder.CreateCylinder("cylinder", {
            height: height,
            diameter: 12,
            diameterBottom: 13,
            tessellation: 4
        }, this.scene);
        var wood = new BABYLON.StandardMaterial("wood", this.scene);
        wood.diffuseTexture = new BABYLON.Texture("https://raw.githubusercontent.com/sballance/BabylonJsTextures/master/textures/dark_wood.jpg", this.scene);
        boardBase.material = wood;
        tiledBoard.position.y = 0;
        boardBase.position.y = -(height / 2 + .0001);
        boardBase.rotation.y = Math.PI / 4;
        this.mesh = boardBase;
    }

    buildSquaresMatrix() {
        let squares = [];
        let x = -3.5;
        let z = -3.5;
        for (var i = 0; i < 8; i++) {
            let row = [];
            for (var j = 0; j < 8; j++) {
                row[j] = {
                    x: x,
                    z: z,
                    piece: null
                }
                z++;
            }
            squares[i] = row;
            x++;
            z = -3.5;
        }
        return squares;
    }

    findSquareByXZ(x, z) {
        for (var xIndex = 0; xIndex < this.squaresMatrix.length; xIndex++) {
            var row = this.squaresMatrix[xIndex];
            for (var zIndex = 0; zIndex < row.length; zIndex++) {
                var square = row[zIndex];
                if (square.x === Math.round(x*10)/10 && square.z === Math.round(z*10)/10) {
                    return [square, zIndex, xIndex];
                }
            }
        }
    }

    findClosestSquare(xClick, zClick) {
        xClick = Math.round(xClick*10)/10;
        zClick = Math.round(zClick*10)/10;
        for (var x = 0; x < this.squaresMatrix.length; x++) {
            var row = this.squaresMatrix[x];
            for (var z = 0; z < row.length; z++) {
                var square = row[z];

                // find spot that is within .5 in x and z direction
                if (Math.abs(square.x - xClick) < .5
                    && Math.abs(square.z - zClick) < .5
                ) {
                    return [square, z, x];
                }
            }
        }
    }

    findSquareAllowsPlay(xIndex, zIndex) {
        return (xIndex + zIndex) % 2 === 0;
    }

    popPieceFromSquare(xIndex, zIndex) {
        let piece = this.squaresMatrix[zIndex][xIndex].piece;
        this.squaresMatrix[zIndex][xIndex].piece = null;
        return piece;
    }

    popPieceFromSquareByXZ(x, z) {
        let square, xIndex, zIndex;
        [square, xIndex, zIndex] = this.findSquareByXZ(x, z);
        let piece = this.popPieceFromSquare(xIndex, zIndex);
        return piece;
    }

    addPieceToSquare(xIndex, zIndex, piece) {
        this.squaresMatrix[zIndex][xIndex].piece = piece;
    }

    addPieceToSquareByXZ(x, z, piece) {
        let square, xIndex, zIndex;
        [square, xIndex, zIndex] = this.findSquareByXZ(x, z);
        this.squaresMatrix[zIndex][xIndex].piece = piece;
    }
}