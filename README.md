# Getting started / How to view the project

Make sure that you have node.js and yarn or npm installed on your machine.Open up this directory in a terminal and run the following commands for yarn:
```
yarn install
yarn prod
```
or for npm:
```
npm install
npm run prod
```

Then simply open the `checkers.html` file in a web browser. I used Chrome but it should work with any modern browser because I am using webpack which should handle any script differences. The `prod` command compiles the script files into the single file, `./dist/main.js`.

# How to play the game

The team closest to the user begins the game. To make a move, click on a piece and then click on a valid place to move it. When jumping a piece, hit enter when you are done with your turn because you don't have to continue jumping pieces if you choose not to. All rules are the same as regular checkers. You become a king when you get across the board, allowing you to move in any direction. The game is over when only one team is left.